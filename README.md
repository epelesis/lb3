# Lightning Bug v3

## Database
`./initdb.sh`

## AssemblyScript
`npm install -g assemblyscript`

## Run
`RUST_BACKTRACE=1  RUST_LOG=info,regalloc=warn,sqlx=warn cargo run`

## Prepare Example
`base64 examples/hello.ts | pbcopy`
