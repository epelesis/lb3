export declare function __put(key: ArrayBuffer, value: ArrayBuffer): void
export declare function __get(key: ArrayBuffer): ArrayBuffer | null

function put(key: string, value: string): void {
    __put(String.UTF8.encode(key), String.UTF8.encode(value));
}

function get(key: string): string | null {
    const result_ = __get(String.UTF8.encode(key));
    if (result_) {
        return String.UTF8.decode(result_);
    } else {
        return null;
    }
}

export const ARRAY_BUFFER_ID = idof<ArrayBuffer>();

export function entry_call_indirect(index: u32, args: Array<ArrayBuffer>): ArrayBuffer {
    const string_args = new Array<string>(args.length);
    for (let i = 0; i < args.length; i++) {
        string_args[i] = String.UTF8.decode(args[i]);
    }

    const result = call_indirect<string>(index, string_args);
    return String.UTF8.encode(result);
}

export function identity(a: string): string {
    return a;
}

export function updateLastSeenUser(user: string): string {
    const lastUserResult = get("lastUser");
    const lastUser = lastUserResult ? lastUserResult : "You are the first user!";

    put("lastUser", user);

    return lastUser;
}
