#!/usr/bin/env bash

set -e
set -x

# source .env

rm dev.db

sqlx db create
sqlx migrate run
