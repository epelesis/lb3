mod compiler;
mod environment;
mod functions;
mod processes;
mod runtime;

// #[macro_use]
extern crate dotenv_codegen;

#[async_std::main]
async fn main() -> anyhow::Result<(), std::io::Error> {
    pretty_env_logger::init();

    let env = environment::Environment::new().await.unwrap();

    let mut app = tide::with_state(env);

    app.at("/functions")
        .post(crate::functions::routes::create_function);
    app.at("/functions/:id")
        .get(crate::functions::routes::find_function);

    app.at("/processes")
        .post(crate::processes::routes::create_process);
    app.at("/processes/:id")
        .get(crate::processes::routes::find_process);

    app.listen("0.0.0.0:3000").await?;
    Ok(())
}
