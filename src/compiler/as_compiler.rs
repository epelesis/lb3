use std::str;

use crate::compiler::WASMFunction;
use crate::functions::model::Function;
use anyhow::anyhow;
use std::{
    env::temp_dir,
    fs::File,
    io::Write,
    path::{Path, PathBuf},
    process::Command,
};

pub struct AsCompiler {}

impl AsCompiler {
    pub fn new() -> anyhow::Result<Self> {
        Ok(Self {})
    }

    pub fn make_wasm(&self, function: &Function) -> anyhow::Result<WASMFunction> {
        let mut function_file = tempfile::Builder::new().suffix(".ts").tempfile()?;
        function_file.write_all(function.contents.as_bytes())?;

        let output = Command::new("asc")
            .arg(function_file.path())
            .arg("--exportRuntime")
            .arg("--use")
            .arg("abort=")
            .output()?;

        let wat_contents_bytes = if output.status.success() {
            Ok(output.stdout)
        } else {
            Err(anyhow!("Failed to compile: Error: {:?}", output))
        }?;

        let wat_contents = str::from_utf8(&wat_contents_bytes)?;

        Ok(WASMFunction {
            wat: wat_contents.to_string(),
        })
    }
}
