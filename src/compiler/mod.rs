use std::path::PathBuf;

pub mod as_compiler;

#[derive(Debug, Clone)]
pub struct WASMFunction {
    pub wat: String,
}

impl WASMFunction {
    pub fn new(wat: String) -> Self {
        Self { wat }
    }
}
