use std::sync::Arc;

use crate::{compiler::as_compiler::AsCompiler, runtime::AsRuntime};
use dotenv_codegen::dotenv;
use sqlx::SqlitePool;

#[derive(Clone)]
pub struct Environment {
    pub pool: SqlitePool,
    pub compiler: Arc<AsCompiler>,
    pub runtime: Arc<AsRuntime>,
    pub redis_client: Arc<redis::Client>,
}

impl Environment {
    #[allow(dead_code)]
    pub async fn new() -> anyhow::Result<Environment> {
        let pool = SqlitePool::connect(dotenv!("DATABASE_URL")).await?;
        let compiler = AsCompiler::new()?;
        let runtime = AsRuntime::new()?;
        let redis_client = redis::Client::open("redis://127.0.0.1/")?;

        Ok(Self {
            pool,
            compiler: Arc::new(compiler),
            runtime: Arc::new(runtime),
            redis_client: Arc::new(redis_client),
        })
    }
}
