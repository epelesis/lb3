use crate::environment::Environment;
use crate::functions::routes::CreateFunctionDto;
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};
use sqlx::FromRow;

#[derive(FromRow, Debug, Serialize, Deserialize)]
pub struct Function {
    pub id: i64,
    pub created: NaiveDateTime,
    pub filename: String,
    pub contents: String,
}

// TODO: Move to impl block?

pub async fn create_function(
    env: &Environment,
    create_function: &CreateFunctionDto,
) -> anyhow::Result<i64> {
    let contents = base64::decode(create_function.base64_encoded_contents.clone())?;

    let id = sqlx::query!(
        r#"
        INSERT INTO functions (filename, contents) VALUES (?, ?)
        "#,
        create_function.filename,
        contents,
    )
    .execute(&env.pool)
    .await?
    .last_insert_rowid();

    Ok(id)
}

pub async fn find_function(env: &Environment, id: i64) -> anyhow::Result<Option<Function>> {
    let function = sqlx::query_as!(
        Function,
        r#"
        SELECT *
        FROM functions
        WHERE id = ?
        "#,
        id
    )
    .fetch_optional(&env.pool)
    .await?;

    Ok(function)
}
