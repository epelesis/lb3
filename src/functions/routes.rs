use crate::environment::Environment;
use crate::functions::model;
use serde::{Deserialize, Serialize};
use tide::{Body, Request, StatusCode};

#[derive(Serialize, Deserialize, Debug)]
pub struct CreateFunctionDto {
    pub filename: String,
    pub base64_encoded_contents: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct CreateFunctionRes {
    pub id: i64,
}

#[allow(dead_code)]
pub async fn create_function(mut req: Request<Environment>) -> tide::Result<Body> {
    let dto: CreateFunctionDto = req.body_json().await?;
    let id = model::create_function(req.state(), &dto).await?;

    // Compile into a runable function
    let function = model::find_function(req.state(), id).await?;

    Body::from_json(&CreateFunctionRes { id })
}

#[allow(dead_code)]
pub async fn find_function(req: Request<Environment>) -> tide::Result<Body> {
    let id: i64 = req.param("id")?.parse()?;

    model::find_function(req.state(), id)
        .await?
        .ok_or(tide::Error::from_str(StatusCode::NotFound, "Not Found"))
        .and_then(|f| Body::from_json(&f))
}
