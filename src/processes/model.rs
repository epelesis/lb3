use crate::environment::Environment;
use crate::processes::routes::CreateProcessDto;
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};
use sqlx::FromRow;

#[derive(FromRow, Debug, Serialize, Deserialize)]
pub struct Process {
    pub id: i64,
    pub created: NaiveDateTime,
    pub function_id: i64,
}

pub async fn create_process(
    env: &Environment,
    create_process: &CreateProcessDto,
) -> anyhow::Result<i64> {
    let id = sqlx::query!(
        r#"
        INSERT INTO processes (function_id) VALUES (?)
        "#,
        create_process.function_id
    )
    .execute(&env.pool)
    .await?
    .last_insert_rowid();

    Ok(id)
}

pub async fn find_process(env: &Environment, id: i64) -> anyhow::Result<Option<Process>> {
    let process = sqlx::query_as!(
        Process,
        r#"
        SELECT *
        FROM processes
        WHERE id = ?
        "#,
        id
    )
    .fetch_optional(&env.pool)
    .await?;

    Ok(process)
}
