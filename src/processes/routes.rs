use crate::compiler::WASMFunction;
use crate::environment::Environment;
use crate::functions;
use crate::processes::model;
use anyhow::{anyhow, Result};
use redis::AsyncCommands;
use serde::{Deserialize, Serialize};
use tide::{Body, Request, StatusCode};

#[derive(Serialize, Deserialize, Debug)]
pub struct CreateProcessDto {
    pub function_id: i64,
    pub target: String,
    pub arguments: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct CreateProcessRes {
    pub id: i64,
    pub result: String,
}

#[allow(dead_code)]
pub async fn create_process(mut req: Request<Environment>) -> tide::Result<Body> {
    let dto: CreateProcessDto = req.body_json().await?;
    let id = model::create_process(req.state(), &dto).await?;

    // TODO: Only lookup function on cache miss
    let function = functions::model::find_function(req.state(), dto.function_id)
        .await?
        .ok_or(anyhow!("Couldn't Find {}", dto.function_id))?;

    let mut redis_conn = req.state().redis_client.get_async_connection().await?;
    let maybe_wat: Option<String> = redis_conn.get(dto.function_id).await?;

    let wasm_function = match maybe_wat {
        Some(wat) => WASMFunction::new(wat),
        None => {
            let wasm_function = req.state().compiler.make_wasm(&function)?;
            redis_conn
                .set(dto.function_id, wasm_function.wat.clone())
                .await?;
            wasm_function
        }
    };

    let result = req
        .state()
        .runtime
        .run(&wasm_function, dto.target, dto.arguments)?;

    Body::from_json(&CreateProcessRes { id, result })
}

#[allow(dead_code)]
pub async fn find_process(req: Request<Environment>) -> tide::Result<Body> {
    let id: i64 = req.param("id")?.parse()?;

    model::find_process(req.state(), id)
        .await?
        .ok_or(tide::Error::from_str(StatusCode::NotFound, "Not Found"))
        .and_then(|f| Body::from_json(&f))
}
