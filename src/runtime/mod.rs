use std::{convert::TryInto, sync::Arc};

use crate::compiler::WASMFunction;
use anyhow::{anyhow, Result};
use async_std::sync::{Mutex, RwLock};
use std::collections::HashMap;
use wasmtime::{Config, Engine, Func, Global, Instance, Module, Store, TypedFunc, Val};

// usize type in Assemblyscript
type USize = i32;

struct KVStore {
    store: Arc<RwLock<HashMap<String, String>>>,
}

impl KVStore {
    pub fn new() -> Self {
        Self {
            store: Arc::new(RwLock::new(HashMap::new())),
        }
    }

    pub async fn get(&self, key: &str) -> Option<String> {
        let store = self.store.read().await;
        store.get(key).map(|s| s.to_string())
    }

    pub async fn put(&self, key: String, value: String) {
        let mut store = self.store.write().await;
        store.insert(key, value);
    }
}

pub struct AsRuntime {
    engine: Engine,
    store: KVStore,
}

#[repr(C, packed)]
#[derive(Debug, Copy, Clone)]
struct CommonHeader {
    pub mm_info: USize,
    pub gc_info: USize,
    pub gc_info2: USize,
    pub rt_id: USize,
    pub rt_size: u32,
}

impl CommonHeader {
    pub fn new(id: USize, size: u32) -> Self {
        Self {
            mm_info: 0,
            gc_info: 0,
            gc_info2: 0,
            rt_id: id,
            rt_size: size,
        }
    }
}

struct ArrayBuffer {
    pub header: CommonHeader,
    pub bytes: Vec<u8>,
}

pub struct AsInstance {
    instance: Instance,
    array_buffer_id: USize,
    __gc_new: TypedFunc<(USize, USize), USize>,
    __gc_pin: TypedFunc<USize, USize>,
}

impl AsInstance {
    pub fn new(instance: Instance) -> Result<AsInstance> {
        let array_buffer_id = instance
            .get_global("ARRAY_BUFFER_ID")
            .ok_or(anyhow!("Failed to load ARRAY_BUFFER_ID"))?
            .get()
            .unwrap_i32();

        let __gc_new = instance.get_typed_func::<(USize, USize), USize>("__new")?;
        let __gc_pin = instance.get_typed_func::<USize, USize>("__pin")?;

        Ok(AsInstance {
            instance,
            array_buffer_id,
            __gc_new,
            __gc_pin,
        })
    }

    pub fn call(&self, target: String, arguments: Vec<String>) -> Result<String> {
        let raw_pointers: Result<Vec<USize>> = arguments
            .into_iter()
            .map(|s| self.__new_string(s))
            .collect();

        let val_pointers: Vec<Val> = raw_pointers?.into_iter().map(|ptr| Val::I32(ptr)).collect();

        let function = self
            .instance
            .get_func(&target)
            .ok_or(anyhow!("Failed to find {}", target))?;

        let return_ptr: USize = function
            .call(&val_pointers)?
            .get(0)
            .ok_or(anyhow!("Failed to call {}", target))?
            .unwrap_i32();

        self.__from_string(return_ptr)
    }

    fn __new_string(&self, s: String) -> Result<USize> {
        let bytes = s.as_bytes();
        let ptr = self.__pin(self.__new(bytes.len() as USize, self.array_buffer_id)?)?;
        self.instance
            .get_memory("memory")
            .ok_or(anyhow!("Couldn't Get Memory"))?
            .write(ptr as usize, s.as_bytes())?;

        Ok(ptr)
    }

    fn __get_header(&self, ptr: USize) -> Result<CommonHeader> {
        let common_header_size_bytes = std::mem::size_of::<CommonHeader>();
        let mut buf: Vec<u8> = vec![0; common_header_size_bytes];

        self.instance
            .get_memory("memory")
            .ok_or(anyhow!("Couldn't Get Memory"))?
            .read(ptr as usize - common_header_size_bytes, &mut buf)?;

        let (head, body, _tail) = unsafe { buf.align_to::<CommonHeader>() };
        assert!(head.is_empty(), "Data was not aligned");

        Ok(body[0])
    }

    fn __from_string(&self, ptr: USize) -> Result<String> {
        let header = self.__get_header(ptr)?;
        assert!(header.rt_id == self.array_buffer_id);
        let mut buf: Vec<u8> = vec![0; header.rt_size as usize];

        self.instance
            .get_memory("memory")
            .ok_or(anyhow!("Couldn't Get Memory"))?
            .read(ptr as usize, &mut buf)?;

        Ok(std::str::from_utf8(&buf)?.to_string())
    }

    fn __new(&self, size: USize, id: USize) -> Result<USize> {
        Ok(self.__gc_new.call((size, id))?)
    }

    fn __pin(&self, ptr: USize) -> Result<USize> {
        Ok(self.__gc_pin.call(ptr)?)
    }
}

impl AsRuntime {
    pub fn new() -> Result<Self> {
        let cfg = Config::new();
        let store = KVStore::new();

        Ok(Self {
            engine: Engine::new(&cfg)?,
            store,
        })
    }

    pub fn run(
        &self,
        wasm_function: &WASMFunction,
        target: String,
        arguments: Vec<String>,
    ) -> Result<String> {
        let store = Store::new(&self.engine);

        let module = Module::new(&store.engine(), &wasm_function.wat)?;

        let instance: Instance = Instance::new(&store, &module, &[])?;

        let as_instance = AsInstance::new(instance)?;

        as_instance.call(target, arguments)
    }
}
